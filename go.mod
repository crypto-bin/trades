module github.com/clebi/binapi

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/segmentio/kafka-go v0.4.10
	github.com/sirupsen/logrus v1.8.0
)
