FROM golang:1.16 AS builder

WORKDIR /tmp/build

COPY . .
RUN go build

FROM golang:1.16

COPY --from=builder /tmp/build/binapi /usr/bin/binapi

USER 1000
CMD [ "/usr/bin/binapi" ]
